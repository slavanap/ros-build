#!/bin/bash
set -e

read_a() {
    A=()
    local line
    while IFS='' read -r line; do
        A+=( "$line" )
    done
}

print_a() {
    local x
    for x in "$@"; do echo "$x"; done
}

unique() {
    print_a "$@" | sort -u
}

get_deps() {
    local line

    local QUERY=()
    if ! source "/opt/ros/$ROS_DISTRO/setup.bash" 2>/dev/null; then
        QUERY+=( "ros-$ROS_DISTRO-catkin" )
    fi

    local ARGS=()
    while IFS='' read -r line; do
        ARGS+=( --from-paths "$line" )
    done < <( unique "$@" )

    local TMP="$(mktemp -d)"
    rosdep check "${ARGS[@]}" --ignore-src --rosdistro "$ROS_DISTRO" 2>"$TMP/err" >"$TMP/out" || cat "$TMP/err" "$TMP/out"

    DEB=()
    local src
    for src in \
        $( sed -rne "s/^ERROR\\[.*\\]: Cannot locate rosdep definition for \\[(.*)\\]\$/\1/p" "$TMP/err" ) \
        $( sed -rne "s/^ERROR\\[.*\\]: No definition of \\[(.*)\\] for OS version \\[.*\\]\$/\1/p" "$TMP/err" )
    do
        QUERY+=( "ros-$ROS_DISTRO-${src//_/-}" )
    done

    local deb
    for deb in $( tail -n +2 "$TMP/out" | grep ^apt | cut -f2 ); do
        if [[ "$deb" == "ros-$ROS_DISTRO-"* ]]; then
            QUERY+=( "$deb" )
        else
            DEB+=( "$deb" )
        fi
    done

    SRC=()
    while IFS='' read -r line; do
        if [[ "$line" == "N: Unable to locate package "* ]]; then
            deb="${line#"N: Unable to locate package "}"

            local NEW_QUERY=()
            for q in "${QUERY[@]}"; do
                if [[ "$q" != "$deb" ]]; then
                    NEW_QUERY+=("$q")
                fi
            done
            QUERY=( "${NEW_QUERY[@]}" )

            DEB=("${DEB[@]/"$deb"}")
            src="${deb#ros-$ROS_DISTRO-*}"
            src="${src//-/_}"
            SRC+=( "$src" )
        fi
    done < <( apt-cache -q=0 show "${QUERY[@]}" 2>&1 || true )

    DEB+=( "${QUERY[@]}" )

    rm -r "$TMP"

    # TODO: remove workarounds
    local NEW_DEB=()
    local lsb_release_name="$(lsb_release -sc)"
    local arch_name="$(dpkg --print-architecture)"
    for deb in "${DEB[@]}"; do
        NEW_DEB+=( "$deb" )
    done
    DEB=( "${NEW_DEB[@]}" )

    local NEW_SRC=()
    for src in "${SRC[@]}"; do
        if [[ "$src" == libqt_opengl_dev ]]; then
            DEB+=( libqt5opengl5-dev )
        elif [[ "$src" == moveit_msgs ]]; then
            :
        elif [[ "$src" == orocos_kdl ]]; then
            DEB+=( liborocos-kdl-dev )
        elif [[ "$src" == xpp_vis && "$lsb_release_name" == focal && "$arch_name" == amd64 ]]; then
            :
        else
            NEW_SRC+=( "$src" )
        fi
    done
    SRC=( "${NEW_SRC[@]}" )
 
 
    local A
    read_a < <( unique "${DEB[@]}" )
    DEB=( "${A[@]}" )
    read_a < <( unique "${SRC[@]}" )
    SRC=( "${A[@]}" )
}

wstool_wrapper() {
    if [[ ! -d "$1" ]]; then
        mkdir -p "$1"
    fi

    vcs import "$1" < "$2"
}

case "$1" in
    deb)
        shift
        get_deps "$@"
        echo
        echo "## Binary dependencies:"
        print_a "${DEB[@]}"
        echo
        echo "## Source dependencies:"

        print_a "${SRC[@]}"
        if (( ${#DEB[@]} > 0 )); then
            echo
            echo "Installing binary dependencies..."
            apt-get install -qqy --no-install-recommends "${DEB[@]}"
            print_a "${DEB[@]}" >> "$DEPS_FILE"
        fi
        ;;
    src)
        shift
        ARGS=( "$@" )
        if [[ -d src ]]; then
            ARGS+=( src )
        fi
        get_deps "${ARGS[@]}"
        echo
        echo "## Binary dependencies:"
        print_a "${DEB[@]}"
        echo
        echo "## Source dependencies:"
        print_a "${SRC[@]}"

        FORCE_DEBS=()
        for deb in "${DEB[@]}"; do
            if [[ "$deb" == "ros-$ROS_DISTRO-"* ]]; then
                src="${deb#ros-$ROS_DISTRO-*}"
                src="${src//-/_}"
                SRC+=( "$src" )
            else
                echo "WARNING: Source files for binary dependency '$deb' not found!" >&2
                FORCE_DEBS+=( "$deb" )
            fi
        done
        if (( ${#SRC[@]} > 0 )); then
            echo
            echo "Downloading source dependencies and source files for binary dependencies..."
            EXCLUDE=""
            if [[ -f "/opt/ros/$ROS_DISTRO/setup.bash" ]]; then
                EXCLUDE="--exclude RPP"
            fi
            TMP="$(mktemp)"
            rosinstall_generator $EXCLUDE --rosdistro "$ROS_DISTRO" --deps --wet-only --tar "${SRC[@]}" > "$TMP"
            wstool_wrapper src "$TMP"

            while :; do
                get_deps "${ARGS[@]}" src
                for deb in "${DEB[@]}"; do
                    if [[ "$deb" == "ros-$ROS_DISTRO-"* ]]; then
                        src="${deb#ros-$ROS_DISTRO-*}"
                        src="${src//-/_}"
                        SRC+=( "$src" )
                    else
                        #echo "WARNING: Source files for binary dependency '$deb' not found!" >&2
                        FORCE_DEBS+=( "$deb" )
                    fi
                done

                read_a < <( print_a "${SRC[@]}" | grep -v "^catkin\$" )
                SRC=( "${A[@]}" )
                if (( ${#SRC[@]} == 0 )); then
                    break
                fi

                echo
                echo "## Additional source dependencies:"
                print_a "${SRC[@]}"
                echo
                echo "Downloading..."
                rosinstall_generator $EXCLUDE --rosdistro "$ROS_DISTRO" --deps --wet-only --tar "${SRC[@]}" > "$TMP"
                wstool_wrapper src "$TMP"
            done
            rm "$TMP"
        fi
        ;;
    *)
        exit 1
        ;;
esac
