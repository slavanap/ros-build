set(CMAKE_CROSSCOMPILING TRUE)
set(CMAKE_LIBRARY_ARCHITECTURE aarch64-linux-gnu)

set(CMAKE_C_COMPILER /usr/bin/aarch64-linux-gnu-gcc)
set(CMAKE_CXX_COMPILER /usr/bin/aarch64-linux-gnu-g++)

set(__MYFLAGS "")
#set(__MYFLAGS "-marm -march=armv8-a+crc+simd -mno-restrict-it -mfpu=vfp -mno-asm-syntax-unified")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${__MYFLAGS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${__MYFLAGS}")

set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Xlinker -rpath-link=/opt/ros/$ENV{ROS_DISTRO}/lib")
