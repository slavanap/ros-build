#!/bin/bash

if [[ -z ${DEB_MAINTAINER+x} ]]; then
	export DEB_MAINTAINER="Vyacheslav Napadovsky <napadovskiy@gmail.com>"
fi

# Utility. Joins arguments into list with desired separator
join_by () (
	set -e
	FIRST=1
	SEP="$1"
	shift
	for x in "$@"; do
		if [[ ! -z "$x" ]]; then
			if [[ "$FIRST" != 0 ]]; then
				FIRST=0
			elif [[ "$x" == \(* ]]; then
				echo -n " "
			else
				echo -n ", "
			fi
			echo -n "$x"
		fi
	done
)

# Build package from files not listed in other installed deb packages
# Usage: buildpkg "/opt/ros/$ROS_DISTRO" orocos_toolchain
# Expected environment variables:
# * DEB_NAME
# * DEB_VERSION
# * DEB_DESCRIPTION
# * DEB_MAINTAINER
buildpkg() (
	set -e
	PKG_DIR="$(mktemp -d)"
	OLD_LIST="$(mktemp)"
	NEW_LIST="$(mktemp)"
	FILES_PATH="$1"
	DEB_FILE="$2"
	shift; shift
	if [[ -z ${DEB_NAME+x} ]]; then
		DEB_NAME="${DEB_FILE%.*}"
	fi
	DEB_NAME="$(echo "$DEB_NAME" | sed 's/_/-/g')"
	if [[ -z ${DEB_DESCRIPTION+x} ]]; then
		DEB_DESCRIPTION="$DEB_NAME package"
	fi
	if [[ -z ${DEB_VERSION+x} ]]; then
		DEB_VERSION=1.0
	fi
	PKG_DEPENDS="$( dpkg -S "$FILES_PATH" | sed 's/:.*//g' )"
	if [[ ! -z "$PKG_DEPENDS" ]]; then
		dpkg -L $( echo "$PKG_DEPENDS" | sed 's/,//g' ) | grep "^$FILES_PATH" | sort -u > "$OLD_LIST"
	fi
	if [[ ! -z "$1" ]]; then
		PKG_DEPENDS="$(join_by ", " "$@" "$PKG_DEPENDS")"
	fi
	find "$FILES_PATH" | sort -u > "$NEW_LIST"
	declare -a FILELIST
	readarray -t FILELIST < <( diff "$OLD_LIST" "$NEW_LIST" | grep "> $FILES_PATH" | sed 's/^> //g' )
	if [[ ! -z ${DEB_REPLACEFILES+x} ]]; then
		FILELIST+=( "${DEB_REPLACEFILES[@]}" )
	fi
	cp --parents -r "${FILELIST[@]}" "$PKG_DIR"
	mkdir "$PKG_DIR/DEBIAN"
	[[ -d "$PKG_DIR$FILES_PATH/etc" ]] && \
		find "$PKG_DIR$FILES_PATH/etc" -type f -printf "$FILES_PATH/etc/%P\n" > "$PKG_DIR/DEBIAN/conffiles"
	cat > "$PKG_DIR/DEBIAN/control" <<-EOF
		Package: $DEB_NAME
		Version: $DEB_VERSION
		Architecture: $(dpkg --print-architecture)
		Maintainer: $DEB_MAINTAINER
		Installed-Size: $(du -s "$PKG_DIR" | cut -f1)
		Priority: extra
	EOF
	[[ ! -z "$PKG_DEPENDS" ]]  && echo Depends: $PKG_DEPENDS >> "$PKG_DIR/DEBIAN/control"
	[[ ! -z ${DEB_REPLACES+x} ]] && echo Replaces: "$(join_by ", " $DEB_REPLACES)" >> "$PKG_DIR/DEBIAN/control"
	[[ ! -z ${DEB_CONFLICTS+x} ]] && echo Conflicts: "$(join_by ", " $DEB_CONFLICTS)" >> "$PKG_DIR/DEBIAN/control"
	[[ ! -z ${DEB_HOMEPAGE+x} ]] && echo Homepage: $DEB_HOMEPAGE >> "$PKG_DIR/DEBIAN/control"
	echo "Description: ${DEB_DESCRIPTION}" >> "$PKG_DIR/DEBIAN/control"
	[[ -f "preinst" ]] && cp "preinst" "$PKG_DIR/DEBIAN/"
	[[ -f "postinst" ]] && cp "postinst" "$PKG_DIR/DEBIAN/"
	[[ -f "postrm" ]] && cp "postrm" "$PKG_DIR/DEBIAN/"
	dpkg-deb --build "$PKG_DIR" "$DEB_FILE"
	rm -r "$PKG_DIR" "$OLD_LIST" "$NEW_LIST"
)
