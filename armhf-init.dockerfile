ARG guest="arm32v7/debian:buster"
ARG host="debian:buster"

# arm64v8/debian:buster
# aarch64-linux-gnu
# arm64v8-buster-rpi4.cmake

FROM debian:bullseye as emu
RUN apt-get update -qq && DEBIAN_FRONTEND=noninteractive apt-get install -qqy --no-install-recommends qemu-user-static

FROM $guest as guest
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8 DEBIAN_FRONTEND=noninteractive
COPY --from=emu /usr/bin/qemu-* /usr/bin/
RUN apt-get update -qq && apt-get upgrade -qqy && apt-get install -qqy --no-install-recommends build-essential

FROM $host as host
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8 DEBIAN_FRONTEND=noninteractive
ARG compiler="arm-linux-gnueabihf"
RUN apt-get update -qq && apt-get upgrade -qqy && \
    apt-get install -qqy "gcc-$compiler" "g++-$compiler" && \
    rm -rf /usr/share/doc/binutils-aarch64-linux-gnu
    # fix error: cannot replace to directory with file

LABEL maintainer "Vyacheslav Napadovsky" <napadovskiy@gmail.com>

RUN set -e; mkdir /rootgcc; \
    for file in /usr/bin/${compiler}-*; do \
      ln "$file" /rootgcc/"$(basename "$file")"; \
    done
RUN rm -rf /usr/share/doc/binutils-aarch64-linux-gnu /usr/share/doc/binutils-arm-linux-gnueabihf
COPY --from=guest / /
RUN set -e; \
    for file in /rootgcc/*; do \
      if ln /usr/bin/"$(basename "$file")" /tmp/; then \
        rm /usr/bin/"$(basename "$file")"; \
        ln "$file" /usr/bin/; \
        rm "$file"; \
        ln /tmp/"$(basename "$file")" /rootgcc/; \
        rm /tmp/"$(basename "$file")"; \
      else \
        ln "$file" /usr/bin/; \
        rm "$file"; \
      fi; \
    done && ( rmdir /rootgcc || true )

ARG toolchain=arm32v7-buster-rpi4.cmake
COPY $toolchain /opt/
ENV CMAKE_TOOLCHAIN_ARG="-DCMAKE_TOOLCHAIN_FILE=/opt/$toolchain"

RUN set -e; \
  apt-get install -qqy --no-install-recommends lsb-release openssl; \
  if [ "$(lsb_release -sc)" = focal -a "$(dpkg --print-architecture)" = armhf ]; then \
    ln -s /etc/ssl/certs/ca-certificates.crt /usr/lib/ssl/cert.pem; \
    apt-get install -qqy --no-install-recommends git ca-certificates libssl-dev pkg-config; \
    git clone -q -b v3.31.5 --depth=1 https://gitlab.kitware.com/cmake/cmake.git /src/cmake; \
    cd /src/cmake; \
    F="-march=armv7ve -D_FILE_OFFSET_BITS=64"; \
    CFLAGS="$CFLAGS $F" CXXFLAGS="$CXXFLAGS $F" ./bootstrap; \
    make; \
    apt-get install -qqy --no-install-recommends cmake; \
    make install; \
    rm -rf /src/cmake; \
  fi
