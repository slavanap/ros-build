#!/bin/bash
set -e

# Before package build

ln -s /tmp "/opt/ros/$ROS_DISTRO/share/flexbe_onboard/tmp"


# Build Debian package

source "buildpkg.bash"

DEB_NAME="ros-${ROS_DISTRO}-sweetie-bot-base$PKG_SUFFIX"

mkdir package
pushd package
PUBLISHER_GUI="/opt/ros/$ROS_DISTRO/lib/python3/dist-packages/joint_state_publisher_gui/__init__.py"
DIVERT_INTRODUCED_VERSION="0.5-7882921606"
if true; then
    cat >preinst <<EOF
#!/bin/bash -e
if [ upgrade != "\$1" ] || dpkg --compare-versions "\$2" lt "$DIVERT_INTRODUCED_VERSION"
then
    dpkg-divert --package "$DEB_NAME" --add --rename --divert "$PUBLISHER_GUI.real" "$PUBLISHER_GUI"
fi
EOF
    cat >postrm <<EOF
#!/bin/bash -e
if [ remove = "\$1" -o abort-install = "\$1" -o disappear = "\$1" ] || \
   [ abort-upgrade = "\$1" ] && dpkg --compare-versions "\$2" lt "$DIVERT_INTRODUCED_VERSION"
then
    [ -f "$PUBLISHER_GUI" ] && rm "$PUBLISHER_GUI"
    dpkg-divert --package "$DEB_NAME" --rename --remove "$PUBLISHER_GUI"
fi
EOF
    cat >postinst <<OUTER_EOF
#!/bin/bash -e
[ -f "$PUBLISHER_GUI.real" ] && cp "$PUBLISHER_GUI.real" "$PUBLISHER_GUI"
cd $(dirname $PUBLISHER_GUI)
patch -Np1 <<EOF
$( cat ../joint_state_publisher_gui.patch )
EOF
OUTER_EOF
    chmod +x preinst postrm postinst
fi

touch "$DEPS_FILE" "$DEPS_FILE.exclude"
for p in $(cat "$DEPS_FILE");         do echo $p; done | sort -u > deps
for p in $(cat "$DEPS_FILE.exclude"); do echo $p; done | sort -u > deps.exclude
DEPS="$(diff -u deps deps.exclude  | grep ^-\\w | sed -e 's/^-//')"
DEB_DESCRIPTION="Sweetie Bot Project base package for $ROS_DISTRO"
DEB_CONFLICTS="ros-${ROS_DISTRO}-orogen ros-${ROS_DISTRO}-ocl ros-${ROS_DISTRO}-rtt ros-${ROS_DISTRO}-log4cpp $(cat deps.exclude)"
rm deps.exclude deps
buildpkg "/opt" "sweetie-bot-base.deb" $DEPS
mv "sweetie-bot-base.deb" ..
popd
