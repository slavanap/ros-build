ARG from=registry.gitlab.com/slavanap/ros-build:bionic-tinker
FROM $from

WORKDIR /src/sweetie_bot
COPY sweetie_bot ./src
RUN cd src && touch \
        hmi/sweetie_bot_rviz_interactions/CATKIN_IGNORE \
        hmi/sweetie_bot_joint_trajectory_editor/CATKIN_IGNORE \
        behavior/sweetie_bot_voice/CATKIN_IGNORE \
        behavior/sweetie_bot_clop_generator/CATKIN_IGNORE \
        inc/ifopt/CATKIN_IGNORE \
        inc/ifopt/ifopt_core/CATKIN_IGNORE \
        inc/ifopt/ifopt_ipopt/CATKIN_IGNORE \
        inc/ifopt/ifopt_snopt/CATKIN_IGNORE \
        inc/towr/towr/CATKIN_IGNORE \
        inc/towr/towr_ros/CATKIN_IGNORE

RUN . "/opt/ros/$ROS_DISTRO/setup.sh" \
  && rosdep check --from-paths src --ignore-src --rosdistro "$ROS_DISTRO" 2>roscheck.tmp \
  || true \
  && cat roscheck.tmp >&2 \
#  && ! grep ERROR roscheck.tmp >/dev/null && rm roscheck.tmp \
  && catkin_make_isolated $CMAKE_TOOLCHAIN_ARG --install --install-space "/opt/ros/sweetie_bot" -DCMAKE_BUILD_TYPE=Release \
       -DLOGGER_DEFAULT=LoggerRosout

RUN apt-get install -qq --no-install-recommends xvfb scrot gdb \
 && useradd -ms /bin/bash sweetie && bash -c "echo -e 'sweetie\nsweetie\n' | sudo passwd sweetie" \
 && echo "sweetie ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/sweetie && chmod 0440 /etc/sudoers.d/sweetie \
 && sed -ie "s/ -platform linuxfb:fb=\/dev\/fb. -plugin EvdevKeyboard//g" /opt/ros/sweetie_bot/share/sweetie_bot_proto2_deploy/default/robot_module.launch \
 && chown sweetie:sweetie -R /home/sweetie

USER sweetie
WORKDIR /home/sweetie
RUN rosdep update

ENV USER=sweetie \
    ROS_MASTER_URI=http://testhost:11311 \
    ROSLAUNCH_SSH_UNKNOWN=1 \
    DISPLAY=:99

CMD sudo /etc/init.d/ssh start \
  && ( /usr/bin/Xvfb $DISPLAY -screen 0 1920x1080x24 & ) && sleep 15 \
  && . /opt/ros/sweetie_bot/setup.sh \
  && roslaunch sweetie_bot_deploy flexbe_control.launch run_real:=true robot:=true host:=false

# docker run --network=isolated_nw -itd --name=proto2 testrobot
