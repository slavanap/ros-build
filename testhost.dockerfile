ARG from=registry.gitlab.com/slavanap/ros-build:bionic-amd64
FROM $from

WORKDIR /src/sweetie_bot
COPY sweetie_bot ./src

RUN . "/opt/ros/$ROS_DISTRO/setup.sh" \
 && rosdep check --from-paths src --ignore-src --rosdistro "$ROS_DISTRO" 2>roscheck.tmp \
 || true \
 && cat roscheck.tmp >&2 \
# && ! grep ERROR roscheck.tmp >/dev/null && rm roscheck.tmp \
 && catkin_make_isolated $CMAKE_TOOLCHAIN_ARG --install --install-space "/opt/ros/sweetie_bot" -DCMAKE_BUILD_TYPE=Release \
      -DLOGGER_DEFAULT=LoggerRosout

RUN apt-get install -qqy --no-install-recommends openssh-server xauth sudo \
 && echo "AddressFamily inet" >> /etc/ssh/sshd_config \
 && useradd -ms /bin/bash dev \
 && usermod -aG root dev \
 && mkdir -p /home/dev/build/src \
 && bash -c "echo -e 'dev\ndev\n' | sudo passwd dev" \
 && chown dev:dev -R /home/dev \
 && echo "dev ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/dev \
 && chmod 0440 /etc/sudoers.d/dev

USER dev
WORKDIR /home/dev/build
COPY --chown=dev:dev sweetie_bot_flexbe_behaviors src/sweetie_bot_flexbe_behaviors
COPY --chown=dev:dev sweetie_bot_proto2_movements src/sweetie_bot_proto2_movements
#COPY --chown=dev:dev sweetie_bot_proto3_movements src/sweetie_bot_proto3_movements
COPY --chown=dev:dev sweetie_bot_sounds src/sweetie_bot_sounds

RUN rosdep update \
 && . /opt/ros/sweetie_bot/setup.sh \
 && rosdep check --from-paths src --ignore-src --rosdistro "$ROS_DISTRO" 2>roscheck.tmp \
 && cat roscheck.tmp >&2 && ! grep ERROR roscheck.tmp >/dev/null && rm roscheck.tmp \
 && catkin_make

# for network tests uncomment the line below
#ENV ROS_MASTER_URI=http://testhost:11311

CMD sudo /etc/init.d/ssh start \
 && . devel/setup.sh \
 && roslaunch sweetie_bot_deploy load_param.launch \
 && sudo /etc/init.d/ssh stop

# Connect via SSH to docker container with X11 forwarding enabled and launch:
# source devel/setup.bash; roslaunch sweetie_bot_deploy flexbe_control.launch run_flexbe:=true
