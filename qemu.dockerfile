FROM debian

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8 DEBIAN_FRONTEND=noninteractive
RUN apt-get update -qq \
 && apt-get install -qqy --no-install-recommends \
      build-essential \
      ca-certificates \
      git \
      libglib2.0-dev \
      ninja-build \
      pkg-config \
      python3

RUN git clone -q --depth=1 --recursive https://gitlab.com/qemu-project/qemu.git /src/qemu
WORKDIR /src/qemu/bin/release/native
RUN ../../../configure --static --target-list=arm-linux-user,aarch64-linux-user \
 && make -j$(nproc)

RUN set -e; for f in /src/qemu/bin/release/native/*-linux-user/qemu-*; do cp -v "$f" /usr/bin/"$(basename "$f")"-static; done
