ARG from=ubuntu:focal
FROM $from

ARG ROS_DISTRO=noetic
LABEL maintainer "Vyacheslav Napadovsky" <napadovskiy@gmail.com>



# DO NOT CHANGE THESE COMMENTS
# STAGE 1 (ROS BASE)

WORKDIR /src/base
COPY apt-ros.org.key .
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8 DEBIAN_FRONTEND=noninteractive ROS_DISTRO="$ROS_DISTRO" \
    CFLAGS="$CFLAGS -Wno-psabi -O2" CXXFLAGS="$CXXFLAGS -fext-numeric-literals -Wno-psabi -O2" \
    DEPS_FILE="/src/base/.deps"

RUN apt-get update -qq && apt-get upgrade -qqy \
 && echo "dirmngr gnupg2 lsb-release ruby curl wget ca-certificates build-essential" >> "$DEPS_FILE" \
 && apt-get install -qqy --no-install-recommends $(cat "$DEPS_FILE") \
 && rm -rf /var/lib/apt/lists/*

RUN (apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654 || apt-key add apt-ros.org.key) \
 && echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list \
 && apt-get -qq update \
 && echo "python3-rosdep python3-rosinstall-generator python3-vcstool python3-urllib3 python3-empy" >> "$DEPS_FILE" \
 && apt-get install -qqy --no-install-recommends $(cat "$DEPS_FILE") \
 && rosdep init \
 && rosdep update

COPY orocos_rtt/src orocos_rtt/src
COPY orocos_rtt/*.patch install-deps.sh ./

RUN set -e; \
    case "$(lsb_release -sc)" in \
        xenial) \
            apt-get install -qqy --no-install-recommends software-properties-common; \
            echo "software-properties-common" >> "$DEPS_FILE"; \
            add-apt-repository ppa:timsc/opencv-3.4; \
            apt-get update -qq; \
            ln -s /usr/include/opencv2 /usr/include/opencv; \
            ;; \
    esac; \
    case "$(dpkg --print-architecture)" in \
        arm*) \
            rm -rf orocos_rtt/src/_host orocos_rtt/src/leap_motion orocos_rtt/src/metaruby-release orocos_rtt/src/rviz_textured_quads; \
            ;; \
        amd64) \
            rm -rf orocos_rtt/src/_robot; \
            ;; \
        i386) \
            rm -rf orocos_rtt/src/_robot; \
            patch -d orocos_rtt/src/leap_motion -Np1 < i386-leap_motion.patch; \
            ;; \
    esac; \
    if [ "$(lsb_release -sc)" = buster -a "$(dpkg --print-architecture)" = arm64 ]; then \
      apt-get install -qqy --no-install-recommends libnlopt-cxx-dev; \
      echo "libnlopt-cxx-dev" >> "$DEPS_FILE"; \
    fi; \
    if [ "$(lsb_release -sc)" = focal -a "$(dpkg --print-architecture)" = amd64 ]; then \
      patch -p0 -d orocos_rtt < typelib.patch; \
    fi

WORKDIR /src/base/rosbase
RUN ../install-deps.sh deb ../orocos_rtt/src
RUN ../install-deps.sh src ../orocos_rtt/src
RUN set -e; \
    if [ -d src ]; then \
      ../install-deps.sh deb ../orocos_rtt/src src; \
      if [ -f "/opt/ros/$ROS_DISTRO/setup.sh" ]; then \
        . "/opt/ros/$ROS_DISTRO/setup.sh"; \
        CATKIN=catkin_make_isolated; \
      else \
        CATKIN=src/catkin/bin/catkin_make_isolated; \
      fi; \
      "$CATKIN" $CMAKE_TOOLCHAIN_ARG --install --install-space "/opt/ros/$ROS_DISTRO" -DCMAKE_BUILD_TYPE=Release \
          -DCATKIN_SKIP_TESTING=ON -DCMAKE_CXX_STANDARD=14; \
    fi



# STAGE 2 (OROCOS)

WORKDIR /src/base/orocos_rtt
RUN . "/opt/ros/$ROS_DISTRO/setup.sh" \
 && for p in ../[0-9][0-9]*; do [ -f "$p" ] && patch -p0 < "$p"; done \
 && cp -a src/orocos_kinematics_dynamics/python_orocos_kdl/pybind11/include/pybind11 "/opt/ros/$ROS_DISTRO/include/pybind11" \
 && catkin_make_isolated $CMAKE_TOOLCHAIN_ARG --install --install-space "/opt/ros/$ROS_DISTRO" -DCMAKE_BUILD_TYPE=Release \
      -DCATKIN_SKIP_TESTING=ON -DCMAKE_CXX_STANDARD=11



# STAGE 3 (MISCELLANEOUS)

WORKDIR /src/base/rtt_finale
RUN . "/opt/ros/$ROS_DISTRO/setup.sh" \
 && if [ "$(lsb_release -sc)" = "stretch" ]; then \
      find "/opt/ros/$ROS_DISTRO" -iname 'package.xml' -exec perl -0777 -pi -e 's/^<\?xml[\s\S]*?\?>//gm' "{}" \; ; \
    fi \
 && mkdir src \
 && ( cd src \
   && rosrun rtt_roscomm create_rtt_msgs control_msgs \
 ) \
 && catkin_make_isolated $CMAKE_TOOLCHAIN_ARG --install --install-space "/opt/ros/$ROS_DISTRO" -DCMAKE_BUILD_TYPE=Release \
      -DCATKIN_SKIP_TESTING=ON -DEDITLINE_H=/usr/include/editline.h

WORKDIR /src/base/rbdl
RUN apt-get install -qqy --no-install-recommends git \
 && git clone --depth=1 --recursive https://github.com/rbdl/rbdl src \
 && . "/opt/ros/$ROS_DISTRO/setup.sh" \
 && mkdir build && cd build \
 && CXXFLAGS="$CXXFLAGS -isystem /opt/ros/$ROS_DISTRO/include" \
    cmake ../src -DCMAKE_INSTALL_PREFIX="/opt/ros/$ROS_DISTRO" -DCMAKE_BUILD_TYPE=Release -DCATKIN_SKIP_TESTING=ON \
      -DRBDL_BUILD_ADDON_URDFREADER=ON -DRBDL_USE_ROS_URDF_LIBRARY=OFF \
 && make -j$(nproc) install

COPY soar /src/soar
WORKDIR /src/soar/soar
RUN apt-get install -qqy --no-install-recommends swig default-jdk \
 && echo "swig default-jdk python3-all-dev" >> "$DEPS_FILE" \
 && bash -c ' \
    set -e; \
    PACKAGES=(kernel cli scripts sml_python headers); \
    if [[ "$(dpkg --print-architecture)" != arm* ]]; then \
      PACKAGES+=( sml_java debugger ); \
    fi; \
    python3 ./scons/scons.py "${PACKAGES[@]}" --out="/opt/soar" \
  '

WORKDIR /src/base
RUN set -e \
 && apt-get install -qqy --no-install-recommends lua-filesystem libxtst6 && echo lua-filesystem libxtst6 >> "$DEPS_FILE" \
 && case "$(dpkg --print-architecture)" in \
        armhf|arm64) \
            apt-get install -qqy --no-install-recommends openssh-server sudo; \
            echo openssh-server sudo >> "$DEPS_FILE"; \
            ;; \
    esac

# build .deb package
ARG pkg_suffix
ENV PKG_SUFFIX="$pkg_suffix"
ARG deb_version="0.3"
ENV DEB_VERSION="$deb_version"
COPY package.bash buildpkg.bash ./
RUN ./package.bash
